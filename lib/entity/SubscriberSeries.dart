import 'package:charts_flutter/flutter.dart' as charts;
import 'package:flutter/foundation.dart';

class SubscriberSeries {
  final String fullName;
  final int subscribers;
  final charts.Color barColor;

  SubscriberSeries(
      {@required this.fullName,
        @required this.subscribers,
        @required this.barColor});
}