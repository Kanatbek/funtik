import 'package:flutter/material.dart';
import 'package:funtik/general/CustomFields.dart';

class SettingPage extends StatefulWidget {
  @override
  _SettingPageState createState() => _SettingPageState();
}

class _SettingPageState extends State<SettingPage> {
  TextEditingController emailController = new TextEditingController();

  @override
  void initState() {
    super.initState();
    emailController.text = "konstantin.konstantinopolsky@gmail.com";
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        color: Color.fromRGBO(246, 247, 248, 1),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            customTopBar(),
            customAppBar(),
            mainWidgets(),
          ],
        ),
      ),
    );
  }

  Widget mainWidgets() {
    return Column(
      children: <Widget>[
        Container(
          color: Colors.white,
          height: MediaQuery.of(context).size.height * 0.12,
          padding: const EdgeInsets.only(top: 20, left: 20),
          child: Column(
            children: [
              Container(
                alignment: Alignment.topLeft,
                child: Text(
                  "Пользовательское соглашение",
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 34,
                  ),
                ),
              ),
            ],
          ),
        ),
        Container(
          height: MediaQuery.of(context).size.height * 0.6,
          child: MediaQuery.removePadding(
            context: context,
            removeTop: true,
            child: ListView(
              children: [
                Container(
                  height: 100,
                  padding: EdgeInsets.only(left: 20, right: 20),
                  color: Colors.white,
                  alignment: Alignment.centerLeft,
                  width: MediaQuery.of(context).size.width * 0.7,
                  child: Text(
                    "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas vitae aenean ac sit sed odio. Nullam neque est venenatis cursus ut.",
                    style: TextStyle(
                      fontSize: 15,
                    ),
                  ),
                ),
                Container(
                  padding: EdgeInsets.only(left: 20, right: 20),
                  height: 30,
                  color: Colors.white,
                  child: Text(
                    "Структура документа",
                    style: TextStyle(fontSize: 24, fontWeight: FontWeight.bold),
                  ),
                ),
                Container(
                  padding: EdgeInsets.only(left: 20, right: 20, top: 20),
                  height: 200,
                  color: Colors.white,
                  width: MediaQuery.of(context).size.width * 0.7,
                  child: Text(
                    "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Lacus enim congue dui, aenean tempor tincidunt orci. Odio nisi, quisque sed ut gravida risus, sed et fringilla. Proin odio nibh et dignissim in. A ligula imperdiet lacus, non platea fusce posuere. Orci in ultrices turpis felis nibh aliquam mauris. Volutpat non vulputate volutpat cursus. Ut ut amet cursus lectus venenatis sit. A, gravida sed ipsum risus odio. Ultricies urna iaculis curabitur viverra arcu duis ac diam lacus.",
                    style: TextStyle(fontSize: 15),
                  ),
                ),
                Container(
                  padding: EdgeInsets.only(left: 20, right: 20),
                  height: 30,
                  color: Colors.white,
                  child: Text(
                    "Общие положения",
                    style: TextStyle(fontSize: 24, fontWeight: FontWeight.bold),
                  ),
                ),
                Container(
                  padding: EdgeInsets.only(left: 20, right: 20, top: 20),
                  height: 200,
                  color: Colors.white,
                  width: MediaQuery.of(context).size.width * 0.7,
                  child: Text(
                    "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Lacus enim congue dui, aenean tempor tincidunt orci. Odio nisi, quisque sed ut gravida risus, sed et fringilla. Proin odio nibh et dignissim in. A ligula imperdiet lacus, non platea fusce posuere. Orci in ultrices turpis felis nibh aliquam mauris. Volutpat non vulputate volutpat cursus. Ut ut amet cursus lectus venenatis sit. A, gravida sed ipsum risus odio. Ultricies urna ",
                    style: TextStyle(fontSize: 15),
                  ),
                )
              ],
            ),
          ),
        ),
        Container(
          height: MediaQuery.of(context).size.height * 0.1,
          alignment: Alignment.bottomCenter,
          width: double.infinity,
          child: CustomFields().enterButton(context, "Принять"),
        ),
      ],
    );
  }

  Widget customTopBar() {
    return Container(
      height: 50,
      color: Colors.white,
    );
  }

  Widget customAppBar() {
    return Container(
      height: 64,
      decoration: BoxDecoration(
        color: Colors.white,
      ),
      child: Row(
        mainAxisSize: MainAxisSize.max,
        children: [
          Container(
            width: MediaQuery.of(context).size.width * 0.15,
            child: Center(
              child: InkWell(
                child: Icon(Icons.arrow_back_ios, color: Colors.green),
                onTap: () {
                  Navigator.pop(context);
                },
              ),
            ),
          ),
          Container(
            width: MediaQuery.of(context).size.width * 0.7,
          ),
          Container(
            width: MediaQuery.of(context).size.width * 0.15,
          ),
        ],
      ),
    );
  }
}
