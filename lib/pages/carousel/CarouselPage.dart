import 'package:carousel_pro/carousel_pro.dart';
import 'package:dot_pagination_swiper/dot_pagination_swiper.dart';
import 'package:flutter/material.dart';
import 'package:funtik/general/CustomFields.dart';

class CarouselPage extends StatefulWidget {
  @override
  _CarouselPageState createState() => _CarouselPageState();
}

class _CarouselPageState extends State<CarouselPage> {
  List<Image> images = [
    Image(
      image: AssetImage("assets/icons/vector1.png"),
    ),
    Image(
      image: AssetImage("assets/icons/vector2.png"),
    ),
    Image(
      image: AssetImage("assets/icons/vector3.png"),
    ),
  ];

  int currentIndex = 0;

  final _imageUrls = [
    "https://png.pngtree.com/thumb_back/fw800/back_pic/00/03/35/09561e11143119b.jpg",
    "https://png.pngtree.com/thumb_back/fw800/back_pic/04/61/87/28586f2eec77c26.jpg",
    "https://png.pngtree.com/thumb_back/fh260/back_pic/04/29/70/37583fdf6f4050d.jpg",
    "https://ak6.picdn.net/shutterstock/videos/6982306/thumb/1.jpg"
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        height: double.infinity,
        width: double.infinity,
        color: Colors.white,
        child: Column(
          children: <Widget>[
            Container(
              height: MediaQuery.of(context).size.height * 0.1,
              alignment: Alignment.topRight,
              padding: EdgeInsets.only(right: 20, top: 50),
              child: Text(
                "Пропустить",
                style: TextStyle(fontSize: 17, color: Colors.green),
              ),
            ),
            Container(
              height: MediaQuery.of(context).size.height * 0.8,
              child: DotPaginationSwiper(
                children: <Widget>[
                  Center(child: firstPage()),
                  Center(child: secondPage()),
                  Center(child: thirdPage()),
                ],
              ),
            ),
            Container(
              height: MediaQuery.of(context).size.height * 0.07,
              child: CustomFields().enterButton(context, "Дальше"),
            ),
            Container(
              height: 20,
            ),
          ],
        ),
      ),
    );
  }

  Widget firstPage() {
    return Container(
      child: Column(
        children: [
          Container(
            height: MediaQuery.of(context).size.height * 0.6,
            decoration: new BoxDecoration(
              image: DecorationImage(
                image: AssetImage("assets/icons/vector1.png"),
              ),
            ),
            child: Padding(
              padding: const EdgeInsets.only(top: 120),
              child: Container(
                height: MediaQuery.of(context).size.height * 0.6,
                decoration: new BoxDecoration(
                  image: DecorationImage(
                    image: AssetImage("assets/images/discounts.png"),
                  ),
                ),
                child: Padding(
                  padding: const EdgeInsets.only(bottom: 20),
                  child: Container(
                    height: MediaQuery.of(context).size.height * 0.6,
                    decoration: new BoxDecoration(
                      image: DecorationImage(
                        image: AssetImage("assets/images/image1.png"),
                      ),
                    ),
                  ),
                ),
              ),
            ),
          ),
          Container(
            height: MediaQuery.of(context).size.height * 0.2,
            child: Column(
              children: [
                Text(
                  "Экономь со вкусом",
                  style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
                  textAlign: TextAlign.center,
                ),
                Container(
                  padding: EdgeInsets.only(top: 20),
                  height: 70,
                  child: Text(
                    "Такое случается! Введите email, указанный при регистрации, мы отправим вам письмо.",
                    style: TextStyle(fontSize: 16),
                    textAlign: TextAlign.center,
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  Widget secondPage() {
    return Container(
      child: Column(
        children: [
          Container(
            height: MediaQuery.of(context).size.height * 0.45,
            child: Image(
              image: AssetImage("assets/icons/vector2.png"),
            ),
          ),
          Container(
            height: MediaQuery.of(context).size.height * 0.25,
            child: Column(
              children: [
                Text(
                  "Еда на расстоянии вытянутой руки",
                  style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
                  textAlign: TextAlign.center,
                ),
                Container(
                  padding: EdgeInsets.only(top: 20),
                  height: 70,
                  child: Text(
                    "Такое случается! Введите email, указанный при регистрации, мы отправим вам письмо.",
                    style: TextStyle(fontSize: 16),
                    textAlign: TextAlign.center,
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  Widget thirdPage() {
    return Container(
      child: Column(
        children: [
          Container(
            height: MediaQuery.of(context).size.height * 0.45,
            child: Image(
              image: AssetImage("assets/icons/vector3.png"),
            ),
          ),
          Container(
            height: MediaQuery.of(context).size.height * 0.25,
            child: Column(
              children: [
                Text(
                  "Более 10 000 ресторанов, кафе и баров",
                  style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
                  textAlign: TextAlign.center,
                ),
                Container(
                  padding: EdgeInsets.only(top: 20),
                  height: 70,
                  child: Text(
                    "Такое случается! Введите email, указанный при регистрации, мы отправим вам письмо.",
                    style: TextStyle(fontSize: 16),
                    textAlign: TextAlign.center,
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
