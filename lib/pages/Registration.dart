import 'package:flutter/material.dart';
import 'package:funtik/general/CustomFields.dart';
import 'package:funtik/general/Navigator.dart';
import 'package:funtik/pages/SettingPage.dart';

class Registration extends StatefulWidget {
  @override
  _RegistrationState createState() => _RegistrationState();
}

class _RegistrationState extends State<Registration> {
  TextEditingController nameController = new TextEditingController();
  TextEditingController surnameController = new TextEditingController();
  TextEditingController phoneController = new TextEditingController();
  TextEditingController emailController = new TextEditingController();
  TextEditingController passwordController = new TextEditingController();

  bool checker = true;

  @override
  void initState() {
    super.initState();
    nameController.text = "Константин";
    surnameController.text = "Константинопольский";
    phoneController.text = "+7 707 707 70 70 ";
    emailController.text = "konstantin.konstantinopolsky@gmail.com";
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        color: Color.fromRGBO(246, 247, 248, 1),
        child: SafeArea(
          top: true,
          bottom: false,
          child: Container(
            color: Color.fromRGBO(246, 247, 248, 1),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
//            customTopBar(),
                customAppBar(),
                mainWidgets(),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget mainWidgets() {
    return Expanded(
      child: MediaQuery.removePadding(
        context: context,
        removeTop: true,
        child: ListView(
          children: <Widget>[
            Column(
              children: <Widget>[
                Container(
                  color: Colors.white,
                  height: MediaQuery.of(context).size.height * 0.11,
                  padding: const EdgeInsets.only(top: 20, bottom: 20, left: 20),
                  child: Column(
                    children: [
                      Container(
                        alignment: Alignment.topLeft,
                        child: Text(
                          "Регистрация",
                          style: TextStyle(
                            fontWeight: FontWeight.bold,
                            fontSize: 34,
                          ),
                        ),
                      ),
                      Container(
                        height: 5,
                      ),
                    ],
                  ),
                ),
                customField("Имя", nameController),
                customField("Фамилия", surnameController),
                customPhoneField("Телефон", phoneController),
                customField("Email адрес", emailController),
                customPasswordField("Пароль", passwordController),
                Container(
                  height: 10,
                ),
                checkerFields2(),
                Container(
                  height: MediaQuery.of(context).size.height * 0.1,
                  alignment: Alignment.center,
                  width: double.infinity,
                  child: CustomFields().enterButton(context, "Отправить"),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }

  Widget checkerFields2() {
    return Container(
      child: Column(
        children: <Widget>[
          Row(
            children: [
              Container(
                padding: EdgeInsets.only(left: 20),
                child: InkWell(
                  onTap: () {
                    setState(() {
                      checker = !checker;
                    });
                  },
                  child: Container(
                    decoration: BoxDecoration(
                      shape: BoxShape.circle,
                      border: (checker)
                          ? Border.all(width: 1, color: Colors.white)
                          : Border.all(width: 1, color: Colors.grey),
                      gradient: LinearGradient(
                        begin: Alignment.centerRight,
                        end: Alignment.centerLeft,
                        colors: (checker)
                            ? [Colors.green, Colors.lightGreenAccent]
                            : [Colors.white, Colors.white],
                      ),
                    ),
                    child: Padding(
                      padding: const EdgeInsets.all(3.0),
                      child: checker
                          ? Icon(
                              Icons.check,
                              size: 17.0,
                              color: Colors.white,
                            )
                          : Icon(
                              Icons.remove,
                              size: 17.0,
                              color: Colors.white,
                            ),
                    ),
                  ),
                ),
              ),
              Container(
                width: MediaQuery.of(context).size.width * 0.8,
                height: 60,
                child: Row(
                  children: <Widget>[
                    Container(
                      padding: EdgeInsets.only(left: 30),
                      width: MediaQuery.of(context).size.width * 0.7,
                      child: Column(
                        children: <Widget>[
                          Row(
                            children: <Widget>[
                              Text(
                                "Я принимаю ",
                                style: TextStyle(fontSize: 16),
                              ),
                              InkWell(
                                onTap: () {
                                  Navigator.of(context).push(CustomNavigator()
                                      .navigate(SettingPage()));
                                },
                                child: Text(
                                  "Пользовательское",
                                  style: TextStyle(
                                      fontSize: 16, color: Colors.green),
                                ),
                              ),
                            ],
                          ),
                          Row(
                            children: <Widget>[
                              InkWell(
                                onTap: () {
                                  Navigator.of(context).push(CustomNavigator()
                                      .navigate(SettingPage()));
                                },
                                child: Text(
                                  "соглашение",
                                  style: TextStyle(
                                      fontSize: 16, color: Colors.green),
                                ),
                              ),
                              Text(
                                " и ",
                                style: TextStyle(fontSize: 16),
                              ),
                              InkWell(
                                onTap: () {
                                  Navigator.of(context).push(CustomNavigator()
                                      .navigate(SettingPage()));
                                },
                                child: Text(
                                  "Политику",
                                  style: TextStyle(
                                      fontSize: 16, color: Colors.green),
                                ),
                              )
                            ],
                          ),
                          Row(
                            children: <Widget>[
                              InkWell(
                                onTap: () {
                                  Navigator.of(context).push(CustomNavigator()
                                      .navigate(SettingPage()));
                                },
                                child: Text(
                                  "конфиденциальности",
                                  style: TextStyle(
                                      fontSize: 16, color: Colors.green),
                                ),
                              ),
                            ],
                          ),
                        ],
                      ),
                    )
                  ],
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }

  Widget customField(String title, TextEditingController controller) {
    return Container(
      height: MediaQuery.of(context).size.height * 0.12,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: <Widget>[
          Container(
            alignment: Alignment.bottomLeft,
            width: MediaQuery.of(context).size.width * 0.9,
            height: MediaQuery.of(context).size.height * 0.05,
            padding: EdgeInsets.only(left: 10),
            child: Text(
              title,
              style: TextStyle(fontSize: 16),
            ),
          ),
          Container(
            height: 10,
          ),
          Container(
            alignment: Alignment.bottomLeft,
            width: MediaQuery.of(context).size.width * 0.9,
            height: MediaQuery.of(context).size.height * 0.05,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(8),
              color: Colors.white,
            ),
            child: CustomFields().emailField(controller, context, false, ""),
          ),
        ],
      ),
    );
  }

  Widget customPhoneField(String title, TextEditingController controller) {
    return Container(
      height: MediaQuery.of(context).size.height * 0.12,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: <Widget>[
          Container(
            alignment: Alignment.bottomLeft,
            width: MediaQuery.of(context).size.width * 0.9,
            height: MediaQuery.of(context).size.height * 0.05,
            padding: EdgeInsets.only(left: 10),
            child: Text(
              title,
              style: TextStyle(fontSize: 16),
            ),
          ),
          Container(
            height: 10,
          ),
          Container(
            alignment: Alignment.bottomLeft,
            width: MediaQuery.of(context).size.width * 0.9,
            height: MediaQuery.of(context).size.height * 0.05,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(8),
              color: Colors.white,
            ),
            child: CustomFields().phoneField(controller, context, checker),
          ),
        ],
      ),
    );
  }

  Widget customPasswordField(String title, TextEditingController controller) {
    return Container(
      height: MediaQuery.of(context).size.height * 0.12,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: <Widget>[
          Container(
            alignment: Alignment.bottomLeft,
            width: MediaQuery.of(context).size.width * 0.9,
            height: MediaQuery.of(context).size.height * 0.05,
            padding: EdgeInsets.only(left: 10),
            child: Text(
              title,
              style: TextStyle(fontSize: 16),
            ),
          ),
          Container(
            height: 10,
          ),
          Container(
            alignment: Alignment.bottomLeft,
            width: MediaQuery.of(context).size.width * 0.9,
            height: MediaQuery.of(context).size.height * 0.05,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(8),
              color: Colors.white,
            ),
            child: CustomFields().passwordField(controller, context, true),
          ),
        ],
      ),
    );
  }

  Widget customTopBar() {
    return Container(
      height: 50,
      color: Colors.white,
    );
  }

  Widget customAppBar() {
    return Container(
      height: 64,
      decoration: BoxDecoration(
        color: Colors.white,
      ),
      child: Row(
        mainAxisSize: MainAxisSize.max,
        children: [
          Container(
            width: MediaQuery.of(context).size.width * 0.15,
            child: Center(
              child: InkWell(
                child: Icon(Icons.arrow_back_ios, color: Colors.green),
                onTap: () {
                  Navigator.pop(context);
                },
              ),
            ),
          ),
          Container(
            width: MediaQuery.of(context).size.width * 0.7,
          ),
          Container(
            width: MediaQuery.of(context).size.width * 0.15,
          ),
        ],
      ),
    );
  }
}
