import 'package:flutter/material.dart';
import 'package:funtik/general/Navigator.dart';
import 'package:funtik/pages/carousel/CarouselPage.dart';
import 'package:funtik/pages/profile/MapPage.dart';
import 'package:funtik/pages/profile/MoneyPage.dart';
import 'package:funtik/pages/profile/SettingPage.dart';
import 'package:funtik/yandexMap/screen/MainMapPage.dart';

class ProfilePage extends StatefulWidget {
  @override
  _ProfilePageState createState() => _ProfilePageState();
}

class _ProfilePageState extends State<ProfilePage> {
  int _index = 0;
  List<String> podpiskaCardsTitle = [
    "Москва Гранд",
    "Москва Гранд2",
    "Москва Гранд3"
  ];

  List<String> leaders = ["Кэмерон Дис", "Кэмерон Дис2", "Кэмерон Дис3"];

  List<String> leaderMoney = ["281 ₽", "900 ₽", "1000 ₽"];

  List<String> prices = ["800 ₽", "900 ₽", "1000 ₽"];
  List<String> discount = ["134.74 ₽", "134.74 ₽", "134.74 ₽"];
  List<String> podpiskaCardsDescription = ["2021", "2022", "2023"];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        color: Colors.white,
        child: SafeArea(
          top: true,
          bottom: false,
          child: Container(
            color: Colors.white,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                customAppBar("Профиль"),
                mainWidgets(),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget customAppBar(String title) {
    return Container(
      height: 64,
      decoration: BoxDecoration(
        color: Colors.white,
      ),
      child: Row(
        mainAxisSize: MainAxisSize.max,
        children: [
          Container(
            width: MediaQuery.of(context).size.width * 0.15,
            child: Center(
              child: InkWell(
                child: Icon(Icons.arrow_back_ios, color: Colors.green),
                onTap: () {
                  Navigator.pop(context);
                },
              ),
            ),
          ),
          Container(
            alignment: Alignment.center,
            width: MediaQuery.of(context).size.width * 0.7,
            child: InkWell(
              onTap: () {
                Navigator.pop(context);
              },
              child: Text(
                title,
                style: TextStyle(
                  fontSize: 24,
                  fontWeight: FontWeight.bold,
                ),
              ),
            ),
          ),
          Container(
            width: MediaQuery.of(context).size.width * 0.15,
            height: 22,
            child: InkWell(
              onTap: () {
                Navigator.of(context).push(
                  CustomNavigator().navigate(
                    SettingPage(),
                  ),
                );
              },
              child: Image(
                image: AssetImage("assets/icons/setting.png"),
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget mainWidgets() {
    return Column(
      children: <Widget>[
        title(),
        card(),
//        spaceBreaker(),
        podpiski(),
        listOfPodpisok(),
        listOfLeader(),
      ],
    );
  }

  Widget listOfLeader() {
    return Container(
      height: MediaQuery.of(context).size.height * 0.24,
      child: ListView(
        children: <Widget>[
          Container(
            height: 50,
            alignment: Alignment.topLeft,
            padding: const EdgeInsets.only(top: 20, left: 20),
            child: Text(
              "Рейтинг лидеров",
              style: TextStyle(
                fontWeight: FontWeight.bold,
                fontSize: 20,
              ),
            ),
          ),
          for (var i = 0;
              i < ((leaders.length != null) ? leaders.length : 0);
              i++)
            Container(
              color: Colors.white,
              child: Column(
                children: <Widget>[
                  customExpandable(
                    i,
                  ),
                ],
              ),
            ),
        ],
      ),
    );
  }

  Widget customExpandable(int i) {
    return Container(
      child: Row(
        children: <Widget>[
          Container(
            alignment: Alignment.centerLeft,
            width: MediaQuery.of(context).size.width * 0.2,
            height: 80,
            padding: EdgeInsets.only(left: 20),
            child: Image(
              height: 80,
              image: AssetImage("assets/images/food.png"),
            ),
          ),
          Container(
            alignment: Alignment.centerLeft,
            height: 80,
            width: MediaQuery.of(context).size.width * 0.4,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Container(
                  padding: EdgeInsets.only(left: 20),
                  alignment: Alignment.centerLeft,
                  child: Text(
                    leaders[i],
                    style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
                  ),
                ),
                Container(
                  padding: EdgeInsets.only(left: 20),
                  alignment: Alignment.centerLeft,
                  child: Text(
                    leaderMoney[i],
                    style: TextStyle(fontSize: 16, color: Colors.grey[400]),
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  Widget listOfPodpisok() {
    return Container(
      height: MediaQuery.of(context).size.height * 0.28,
      color: Color.fromRGBO(246, 247, 248, 1),
      child: Container(
        alignment: Alignment.topCenter,
        height: 400,
        child: SizedBox(
//          height: 400, // card height
          child: PageView.builder(
            itemCount: 3,
            controller: PageController(viewportFraction: 0.95),
            onPageChanged: (int index) => setState(() => _index = index),
            itemBuilder: (_, i) {
              return Transform.scale(
                scale: i == _index ? 1 : 0.9,
                child: Column(
                  children: <Widget>[
                    Card(
//                      elevation: 6,
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(10)),
                      child: podpiskaCard(i),
                    ),
                    price(i),
                  ],
                ),
              );
            },
          ),
        ),
      ),
    );
  }

  Widget podpiskaCard(int i) {
    return Container(
      child: Row(
        children: <Widget>[
          Container(
            height: 150,
            width: MediaQuery.of(context).size.width * 0.5,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Container(
                  padding: EdgeInsets.only(left: 20),
                  alignment: Alignment.centerLeft,
                  child: Text(
                    podpiskaCardsTitle[i],
                    style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
                  ),
                ),
                Container(
                  padding: EdgeInsets.only(left: 20),
                  alignment: Alignment.centerLeft,
                  child: Text(
                    podpiskaCardsDescription[i],
                    style: TextStyle(fontSize: 20, color: Colors.grey[400]),
                  ),
                ),
              ],
            ),
          ),
          Container(
            alignment: Alignment.centerRight,
            width: MediaQuery.of(context).size.width * 0.4,
            height: 100,
            child: Image(
              image: AssetImage("assets/images/food.png"),
            ),
          ),
        ],
      ),
    );
  }

  Widget spaceBreaker() {
    return Container(
      height: MediaQuery.of(context).size.height * 0.05,
      color: Colors.white,
    );
  }

  Widget podpiski() {
    return Container(
      height: MediaQuery.of(context).size.height * 0.08,
      color: Color.fromRGBO(246, 247, 248, 1),
      child: Container(
        padding: EdgeInsets.only(left: 20, right: 20),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Text(
              "Подписки",
              style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
            ),
            Text(
              "См. Все",
              style: TextStyle(fontSize: 16, color: Colors.green),
            ),
          ],
        ),
      ),
    );
  }

  Widget price(int i) {
    return Container(
      alignment: Alignment.centerLeft,
      height: MediaQuery.of(context).size.height * 0.08,
      color: Color.fromRGBO(246, 247, 248, 1),
      child: Container(
        padding: EdgeInsets.only(left: 5, right: 20, top: 10),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
//          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Text(
              prices[i],
              style: TextStyle(
                  fontSize: 20,
                  fontWeight: FontWeight.bold,
                  color: Colors.green),
            ),
            Text(
              discount[i],
              style: TextStyle(
                fontSize: 13,
                decoration: TextDecoration.lineThrough,
                fontWeight: FontWeight.bold,
                color: Color.fromRGBO(200, 201, 214, 1),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget card() {
    return Container(
      padding: EdgeInsets.only(bottom: 10,),
      height: MediaQuery.of(context).size.height * 0.17,
      child: InkWell(
        onTap: () {
          Navigator.of(context).push(
            CustomNavigator().navigate(
              MapPage(),
            ),
          );
        },
        child: Container(
          width: MediaQuery.of(context).size.width * 0.9,
          decoration: BoxDecoration(
            gradient: LinearGradient(
              colors: <Color>[
                Color.fromRGBO(151, 223, 71, 1),
                Color.fromRGBO(60, 194, 74, 1),
              ],
            ),
            boxShadow: [
              BoxShadow(
                color: Colors.green.withOpacity(0.4),
                spreadRadius: 5,
                blurRadius: 7,
                offset: Offset(0, 3), // changes position of shadow
              ),
            ],
            borderRadius: BorderRadius.all(
              Radius.circular(10),
            ),
          ),
          child: Container(
            alignment: Alignment.bottomLeft,
            child: Column(
              children: <Widget>[
                Container(
                  padding: EdgeInsets.only(left: 30),
                  height: MediaQuery.of(context).size.height * 0.1,
                  alignment: Alignment.bottomLeft,
                  child: Text(
                    "СБЕРЕЖЕНИЯ",
                    style: TextStyle(
                      fontSize: 20,
                      color: Color.fromRGBO(255, 255, 255, 1),
                    ),
                  ),
                ),
                Container(
                  child: Row(
                    children: <Widget>[
                      Container(
//                      width: MediaQuery.of(context).size.width * 0.5,
                        child: Text(
                          "0 ₽",
                          style: TextStyle(fontSize: 30, color: Colors.white),
                        ),
                        padding: EdgeInsets.only(left: 30),
                      ),
                      Container(
                        width: MediaQuery.of(context).size.width * 0.65,
                        alignment: Alignment.centerRight,
                        child: Icon(
                          Icons.arrow_forward_ios_outlined,
                          color: Colors.white,
                        ),
                        padding: EdgeInsets.only(left: 30),
//                      width: MediaQuery.of(context).size.width * 0.5,
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget title() {
    return Container(
      color: Colors.white,
      height: MediaQuery.of(context).size.height * 0.12,
      child: Row(
        children: <Widget>[
          Container(
            width: MediaQuery.of(context).size.width * 0.7,
            child: Padding(
              padding: const EdgeInsets.all(20),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Container(
                    child: Text(
                      "Андрей Л.",
                      style:
                          TextStyle(fontSize: 24, fontWeight: FontWeight.bold),
                    ),
                    width: double.infinity,
                  ),
                  Container(
                    child: Text(
                      "and.linnik@gmail.com",
                      style: TextStyle(
                        fontSize: 16,
                        color: Colors.grey,
                      ),
                    ),
                    width: double.infinity,
                  ),
                ],
              ),
            ),
          ),
          Container(
            width: MediaQuery.of(context).size.width * 0.3,
            alignment: Alignment.centerRight,
            child: Padding(
              padding: const EdgeInsets.all(20),
              child: InkWell(
                onTap: () {
                  Navigator.of(context).push(
                    CustomNavigator().navigate(
                      CarouselPage(),
                    ),
                  );
                },
                child: CircleAvatar(
                  child: Icon(Icons.person),
                  radius: 30,
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
