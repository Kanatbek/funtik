import 'package:flutter/material.dart';
import 'package:funtik/pages/CategoryPage.dart';
import 'package:modal_bottom_sheet/modal_bottom_sheet.dart';

class MapPage extends StatefulWidget {
  @override
  _MapPageState createState() => _MapPageState();
}

class _MapPageState extends State<MapPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        color: Colors.white,
        child: SafeArea(
          top: true,
          bottom: false,
          child: Container(
            color: Colors.white,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                customAppBar("Сбережения"),
                mainWidget(),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget mainWidget() {
    showMaterialModalBottomSheet(
      context: context,
      builder: (context) => SingleChildScrollView(
        controller: ModalScrollController.of(context),
        child: Container(),
      ),
    );
  }

  Widget customAppBar(String title) {
    return Container(
      height: 64,
      decoration: BoxDecoration(
        color: Colors.white,
      ),
      child: Row(
        mainAxisSize: MainAxisSize.max,
        children: [
          Container(
            width: MediaQuery.of(context).size.width * 0.15,
            child: Center(
              child: InkWell(
                child: Icon(Icons.arrow_back_ios, color: Colors.green),
                onTap: () {
                  Navigator.pop(context);
                },
              ),
            ),
          ),
          Container(
            alignment: Alignment.center,
            width: MediaQuery.of(context).size.width * 0.7,
            child: InkWell(
              onTap: () {
                Navigator.pop(context);
              },
              child: Text(
                title,
                style: TextStyle(
                  fontSize: 24,
                  fontWeight: FontWeight.bold,
                ),
              ),
            ),
          ),
          Container(
            width: MediaQuery.of(context).size.width * 0.15,
            height: 22,
//            child: Image(
//              image: AssetImage("assets/icons/setting.png"),
//            ),
          ),
        ],
      ),
    );
  }
}
