import 'package:flutter/material.dart';
import 'package:funtik/entity/Account.dart';
import 'package:funtik/general/CustomFields.dart';

class FriendPage extends StatefulWidget {
  @override
  _FriendPageState createState() => _FriendPageState();
}

class _FriendPageState extends State<FriendPage> {
//  List<Account> accounts = [
//    new Account("asd asd", 200.0, "asd", null),
//    new Account("asd asd", 200.0, "asd", null),
//    new Account("asd asd", 200.0, "asd", null),
//  ];
  List<Account> accounts = new List<Account>();

  @override
  void initState() {
    super.initState();
    Account a = new Account();
    a.fullName = "Камерон Вильямс";
    a.money = 2.3;
    a.status = "Глава";
    a.image = null;
    accounts.add(a);
    Account a1 = new Account();
    a1.fullName = "Андрей Л.";
    a1.money = 2.3;
    a1.status = "";
    a1.image = null;
    accounts.add(a1);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        color: Colors.white,
        child: SafeArea(
          top: true,
          bottom: false,
          child: Container(
            color: Colors.white,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                customAppBar("Мои друзья"),
                title(),
//                mainWidgets(),
                listOfLeader(),
                Container(
                  height: 50,
                ),
                CustomFields().enterButton2(context, "Пригласить"),
                Container(
                  height: 20,
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget listOfLeader() {
    return Expanded(
      child: Container(
        child: MediaQuery.removePadding(
          context: context,
          removeTop: true,
          child: ListView(
            children: <Widget>[
//              Container(
//                height: 50,
//                alignment: Alignment.topLeft,
//                padding: const EdgeInsets.only(top: 20, left: 20),
//                child: Text(
//                  "Рейтинг лидеров",
//                  style: TextStyle(
//                    fontWeight: FontWeight.bold,
//                    fontSize: 20,
//                  ),
//                ),
//              ),
              addFriend(),
              for (var i = 0;
                  i < ((accounts.length != null) ? accounts.length : 0);
                  i++)
                Container(
                  color: Colors.white,
                  child: Column(
                    children: <Widget>[
                      customExpandable(
                        i,
                      ),
                    ],
                  ),
                ),
            ],
          ),
        ),
      ),
    );
  }

  Widget addFriend() {
    return Container(
      child: Row(
        children: <Widget>[
          Container(
            alignment: Alignment.center,
            width: MediaQuery.of(context).size.width * 0.22,
            height: 55,
            child: CircleAvatar(
              radius: 35,
              backgroundColor: Colors.green,
              child: Icon(
                Icons.add,
                color: Colors.white,
              ),
            ),
          ),
          Container(
            alignment: Alignment.centerLeft,
            height: 100,
            width: MediaQuery.of(context).size.width * 0.77,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Container(
                  padding: EdgeInsets.only(left: 20),
                  alignment: Alignment.centerLeft,
                  child: Text(
                    "Добавить друга",
                    style: TextStyle(fontSize: 20, color: Colors.green),
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  Widget customExpandable(int i) {
    return Container(
      child: Row(
        children: <Widget>[
          Container(
            alignment: Alignment.center,
            width: MediaQuery.of(context).size.width * 0.23,
            height: 80,
            child: Image(
              height: 60,
              image: AssetImage("assets/images/food.png"),
            ),
          ),
          Container(
            alignment: Alignment.centerLeft,
            height: 100,
            width: MediaQuery.of(context).size.width * 0.77,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Container(
                  padding: EdgeInsets.only(left: 20, right: 30, bottom: 5),
                  alignment: Alignment.centerLeft,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(
                        accounts[i]?.fullName,
                        style: TextStyle(
                            fontSize: 16, fontWeight: FontWeight.bold),
                      ),
                      Text(
                        accounts[i]?.status,
                        style: TextStyle(fontSize: 13, color: Colors.grey),
                      ),
                    ],
                  ),
                ),
                Container(
                  padding: EdgeInsets.only(left: 20),
                  alignment: Alignment.centerLeft,
                  child: Text(
                    accounts[i]?.money?.toString(),
                    style: TextStyle(fontSize: 13, color: Colors.grey[400]),
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  Widget title() {
    return Container(
      color: Color.fromRGBO(246, 247, 248, 1),
      height: MediaQuery.of(context).size.height * 0.08,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Container(
            width: MediaQuery.of(context).size.width * 0.6,
            child: Padding(
              padding: const EdgeInsets.only(left: 25),
              child: Container(
                child: Text(
                  "Таблица рейтинга",
                  style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
                ),
                width: double.infinity,
              ),
            ),
          ),
          Container(
            width: MediaQuery.of(context).size.width * 0.4,
            alignment: Alignment.centerRight,
            child: Padding(
              padding: const EdgeInsets.all(20),
              child: Container(
                alignment: Alignment.centerRight,
                child: Text(
                  "${accounts?.length} друзей",
                  style: TextStyle(
                    fontSize: 20,
                    fontWeight: FontWeight.bold,
                    color: Colors.green,
                  ),
                ),
                width: double.infinity,
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget mainWidgets() {
    return Expanded(
      child: Container(
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.all(
            Radius.circular(32.0),
          ),
        ),
        child: MediaQuery.removePadding(
          context: context,
          removeTop: true,
          child: ListView(
            children: <Widget>[
              for (int i = 0; i < accounts.length; i++)
                InkWell(
                  onTap: () {},
                  child: Container(
                    padding: EdgeInsets.only(left: 10),
                    width: MediaQuery.of(context).size.width * 0.8,
                    height: 50,
                    color: Colors.white,
                    alignment: Alignment.centerLeft,
                    child: Text(
                      (accounts[i]?.fullName != null)
                          ? accounts[i]?.fullName
                          : "ss",
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 20,
                      ),
                    ),
                  ),
                ),
            ],
          ),
        ),
      ),
    );
  }

  Widget customAppBar(String title) {
    return Container(
      height: 64,
      decoration: BoxDecoration(
        color: Colors.white,
      ),
      child: Row(
        mainAxisSize: MainAxisSize.max,
        children: [
          Container(
            width: MediaQuery.of(context).size.width * 0.15,
            child: Center(
              child: InkWell(
                child: Icon(Icons.arrow_back_ios, color: Colors.green),
                onTap: () {
                  Navigator.pop(context);
                },
              ),
            ),
          ),
          Container(
            alignment: Alignment.center,
            width: MediaQuery.of(context).size.width * 0.7,
            child: InkWell(
              onTap: () {
                Navigator.pop(context);
              },
              child: Text(
                title,
                style: TextStyle(
                  fontSize: 24,
                  fontWeight: FontWeight.bold,
                ),
              ),
            ),
          ),
          Container(
            width: MediaQuery.of(context).size.width * 0.15,
            height: 22,
//            child: Image(
//              image: AssetImage("assets/icons/setting.png"),
//            ),
          ),
        ],
      ),
    );
  }
}
