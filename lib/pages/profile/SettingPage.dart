import 'package:flutter/material.dart';
import 'package:funtik/general/CustomFields.dart';
import 'package:funtik/general/Navigator.dart';
import 'package:funtik/pages/profile/FamilyPage.dart';
import 'package:funtik/pages/profile/FriendPage.dart';

class SettingPage extends StatefulWidget {
  @override
  _SettingPageState createState() => _SettingPageState();
}

class _SettingPageState extends State<SettingPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        color: Colors.white,
        child: SafeArea(
          top: true,
          bottom: false,
          child: Container(
            color: Colors.white,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                customAppBar("Настройки"),
                mainWidgets(),
                Container(
                  height: 50,
                ),
                CustomFields().enterButton2(context, "Выйти"),
                Container(
                  height: 20,
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget mainWidgets() {
    return Expanded(
      child: Padding(
        padding: const EdgeInsets.only(top: 10),
        child: Container(
          decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.all(
              Radius.circular(32.0),
            ),
          ),
          child: MediaQuery.removePadding(
            context: context,
            removeTop: true,
            child: ListView(
              children: [
                Column(
                  children: <Widget>[
                    CustomFields().customSelector("Мой аккаунт", context),
                    CustomFields().customSelector("Кошелек", context),
                    CustomFields().customTitle("Сообщество", context),
                    InkWell(
                        onTap: () {
                          Navigator.of(context).push(
                            CustomNavigator().navigate(
                              FamilyPage(),
                            ),
                          );
                        },
                        child: CustomFields()
                            .customSelector("Моя семья", context)),
                    InkWell(
                        onTap: () {
                          Navigator.of(context).push(
                            CustomNavigator().navigate(
                              FriendPage(),
                            ),
                          );
                        },
                        child: CustomFields()
                            .customSelector("Мои друзья", context)),
                    CustomFields().customTitle("Способ оплаты", context),
                    CustomFields().customSelector("Свистелки", context),
                    CustomFields().customSelector("Мои подписки", context),
                    CustomFields().customSelector("Погашения", context),
                    CustomFields()
                        .customSelector("История Промо Кодов", context),
                    CustomFields().customSelector("Заказы", context),
                    CustomFields().customTitle("Уведомления", context),
                    CustomFields().customChecker("Push Уведомления", context),
                    CustomFields().customSelector("Рассыка по Email", context),
                    CustomFields().customTitle("Помощь", context),
                    CustomFields().customSelector("Служба поддержки", context),
                    CustomFields().customSelector("О Приложении", context),
                    CustomFields()
                        .customSelector("App version v7.01 (221)", context),
                  ],
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget customAppBar(String title) {
    return Container(
      height: 64,
      decoration: BoxDecoration(
        color: Colors.white,
      ),
      child: Row(
        mainAxisSize: MainAxisSize.max,
        children: [
          Container(
            width: MediaQuery.of(context).size.width * 0.15,
            child: Center(
              child: InkWell(
                child: Icon(Icons.arrow_back_ios, color: Colors.green),
                onTap: () {
                  Navigator.pop(context);
                },
              ),
            ),
          ),
          Container(
            alignment: Alignment.center,
            width: MediaQuery.of(context).size.width * 0.7,
            child: InkWell(
              onTap: () {
                Navigator.pop(context);
              },
              child: Text(
                title,
                style: TextStyle(
                  fontSize: 24,
                  fontWeight: FontWeight.bold,
                ),
              ),
            ),
          ),
          Container(
            width: MediaQuery.of(context).size.width * 0.15,
            height: 22,
//            child: Image(
//              image: AssetImage("assets/icons/setting.png"),
//            ),
          ),
        ],
      ),
    );
  }
}
