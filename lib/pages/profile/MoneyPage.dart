import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:funtik/entity/Account.dart';
import 'package:funtik/entity/SubscriberSeries.dart';
import 'package:funtik/general/CustomBar.dart';
import 'package:funtik/general/CustomFields.dart';
import 'package:charts_flutter/flutter.dart' as charts;
import 'SubscriberChart.dart';

class MoneyPage extends StatefulWidget {
  @override
  _MoneyPageState createState() => _MoneyPageState();
}

class _MoneyPageState extends State<MoneyPage> {
  List<Account> accounts = new List<Account>();
  int segmentedControlGroupValue = 0;
  int selectedMonth = 0;
  final List<SubscriberSeries> data = [
    SubscriberSeries(
      fullName: "22",
      subscribers: 20,
      barColor: charts.ColorUtil.fromDartColor(Colors.blue),
    ),
    SubscriberSeries(
      fullName: "33",
      subscribers: 15,
      barColor: charts.ColorUtil.fromDartColor(Colors.green),
    ),
    SubscriberSeries(
      fullName: "44",
      subscribers: 2,
      barColor: charts.ColorUtil.fromDartColor(Colors.redAccent),
    ),
    SubscriberSeries(
      fullName: "",
      subscribers: 0,
      barColor: charts.ColorUtil.fromDartColor(Colors.yellow),
    ),
    SubscriberSeries(
      fullName: "",
      subscribers: 0,
      barColor: charts.ColorUtil.fromDartColor(Colors.blue),
    ),
    SubscriberSeries(
      fullName: "",
      subscribers: 0,
      barColor: charts.ColorUtil.fromDartColor(Colors.blue),
    ),
  ];

  List<String> leaders = ["Кэмерон Дис", "Кэмерон Дис2", "Кэмерон Дис3"];

  List<String> leaderMoney = ["281 ₽", "900 ₽", "1000 ₽"];

  List<Color> customColors = [
    Colors.green,
    Colors.orange,
    Colors.lightBlueAccent,
  ];

  List<String> monthValues = [
    "ЯНВАРЬ",
    "ФЕВРАЛЬ",
    "МАРТ",
    "АПРЕЛЬ",
    "МАЙ",
    "ИЮНЬ",
    "ИЮЛЬ",
    "АВГУСТ",
    "СЕНТЯБРЬ",
    "ОКТЯБРЬ",
    "НОЯБРЬ",
    "ДЕКАБРЬ"
  ];

  @override
  void initState() {
    super.initState();
    Account a = new Account();
    a.fullName = "Камерон Вильямс";
    a.money = 2.3;
    a.status = "Глава";
    a.image = null;
    accounts.add(a);
    Account a1 = new Account();
    a1.fullName = "Андрей Л.";
    a1.money = 2.3;
    a1.status = "";
    a1.image = null;
    accounts.add(a1);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        color: Colors.white,
        child: SafeArea(
          top: true,
          bottom: false,
          child: Container(
            color: Colors.white,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                customAppBar("Сбережения"),
                switcher(),
                (segmentedControlGroupValue == 0)
                    ? firstSwitch()
                    : secondSwitch(),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget firstSwitch() {
    return Column(
      children: [
        months(),
        totalBudget(),
        chart(),
        Container(
          color: Color.fromRGBO(246, 247, 248, 1),
          child: CustomFields()
              .customChecker("Показать семейные сбережения", context),
        ),
        listOfLeader(0.185),
      ],
    );
  }

  Widget listOfLeader(double customWidth) {
    return Container(
      height: MediaQuery.of(context).size.height * customWidth,
      child: ListView(
        children: <Widget>[
          for (var i = 0;
              i < ((leaders.length != null) ? leaders.length : 0);
              i++)
            Container(
              color: Colors.white,
              child: Column(
                children: <Widget>[
                  customExpandable(
                    i,
                  ),
                ],
              ),
            ),
        ],
      ),
    );
  }

  Widget customExpandable(int i) {
    return Container(
      height: 60,
      child: Row(
        children: <Widget>[
          Container(
            alignment: Alignment.center,
            width: MediaQuery.of(context).size.width * 0.2,
            height: 42,
            child: Image(
              image: AssetImage("assets/images/food.png"),
            ),
          ),
          Container(
            alignment: Alignment.centerLeft,
            height: 150,
            width: MediaQuery.of(context).size.width * 0.8,
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Container(
                  padding: EdgeInsets.only(left: 10),
                  alignment: Alignment.centerLeft,
                  child: Text(
                    leaders[i],
                    style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
                  ),
                ),
                Container(
                  padding: EdgeInsets.only(right: 20),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      Container(
                        padding: EdgeInsets.only(right: 20),
                        alignment: Alignment.centerLeft,
                        child: Text(
                          leaderMoney[i],
                          style: TextStyle(fontSize: 16, color: Colors.black),
                        ),
                      ),
                      Container(
                        height: 18,
                        width: 18,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(4),
                          color: customColors[i],
                        ),
                      ),
                    ],
                  ),
                )
              ],
            ),
          ),
        ],
      ),
    );
  }

  Widget chart() {
    return Container(
      height: MediaQuery.of(context).size.height * 0.4,
      decoration: BoxDecoration(
        color: Color.fromRGBO(246, 247, 248, 1),
        boxShadow: [
          BoxShadow(
            color: Colors.grey.withOpacity(0.2),
            spreadRadius: 5,
            blurRadius: 7,
            offset: Offset(0, 3), // changes position of shadow
          ),
        ],
      ),
      child: SubscriberChart(
        data: data,
      ),
//      child: Row(
//        children: [
//          Container(
//            width: 50,
//            height: MediaQuery.of(context).size.height * 0.4,
//            color: Colors.greenAccent,
//            child: CustomSlider(
//              percentage: 100,
//              width: 30,
//            ),
//          ),
//          Container(
//            width: 50,
//            child: CustomSlider(
//              percentage: 50,
//              width: 40,
//            ),
//          ),
//        ],
//      ),
    );
  }

  Widget customChecker(String title, BuildContext context) {
    return Container(
      padding: EdgeInsets.only(left: 20, right: 20),
      width: MediaQuery.of(context).size.width * 1,
      height: 70,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Padding(
            padding: const EdgeInsets.only(top: 20),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Text(
                  title,
                  style: TextStyle(fontSize: 16),
                ),
                CupertinoSwitch(
                  value: true,
                  onChanged: null,
                ),
              ],
            ),
          ),
          Container(
            height: 5,
            decoration: BoxDecoration(
              border: Border(
                bottom: BorderSide(
                  width: 1.0,
                  color: Color.fromRGBO(246, 247, 248, 1),
                ),
              ),
              color: Colors.white,
            ),
          ),
        ],
      ),
    );
  }

  Widget months() {
    return Container(
      alignment: Alignment.bottomCenter,
      padding: EdgeInsets.only(left: 10),
      height: MediaQuery.of(context).size.height * 0.07,
      color: Color.fromRGBO(246, 247, 248, 1),
      child: Container(
        padding: EdgeInsets.all(12),
        alignment: Alignment.center,
//        height: 30,
        child: ListView(
          scrollDirection: Axis.horizontal,
          children: [
            for (int i = 0; i < monthValues.length; i++)
              Container(
                padding: EdgeInsets.only(right: 10),
                child: Container(
                  padding:
                      EdgeInsets.only(left: 20, right: 20, top: 10, bottom: 10),
                  alignment: Alignment.center,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(32),
                    gradient: LinearGradient(
                      colors: (selectedMonth == i)
                          ? <Color>[
                              Color.fromRGBO(151, 223, 71, 1),
                              Color.fromRGBO(60, 194, 74, 1),
                            ]
                          : <Color>[
                              Colors.white,
                              Colors.white,
                            ],
                    ),
                  ),
                  child: Text(
                    monthValues[i],
                    style: TextStyle(
                        color:
                            (selectedMonth == i) ? Colors.white : Colors.black),
                  ),
                  height: 30,
//                width: 60,
                ),
              ),
          ],
        ),
      ),
    );
  }

  Widget totalBudget() {
    return Container(
      height: MediaQuery.of(context).size.height * 0.07,
      color: Color.fromRGBO(246, 247, 248, 1),
      child: Container(
        padding: EdgeInsets.only(left: 25, right: 20),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Text(
              "Всего сбережений:",
              style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
            ),
            Text(
              "9211 ₽",
              style: TextStyle(
                  fontSize: 20,
                  color: Colors.black,
                  fontWeight: FontWeight.bold),
            ),
          ],
        ),
      ),
    );
  }

  Widget secondSwitch() {
    return Column(
      children: [
        totalBudget(),
        chart(),
        Container(
          color: Color.fromRGBO(246, 247, 248, 1),
          child: CustomFields()
              .customChecker("Показать семейные сбережения", context),
        ),
        listOfLeader(0.255),
      ],
    );
  }

  Widget switcher() {
    return Container(
      height: MediaQuery.of(context).size.height * 0.08,
      alignment: Alignment.center,
      color: Colors.white,
      child: CupertinoSlidingSegmentedControl(
        groupValue: segmentedControlGroupValue,
        backgroundColor: Colors.grey[200],
        thumbColor: Colors.white,
        children: <int, Widget>{
          0: Container(
            width: MediaQuery.of(context).size.width * 0.4,
            height: 35,
            alignment: Alignment.center,
            child: Text(
              "За месяц",
              style: TextStyle(
                  color: (segmentedControlGroupValue == 0)
                      ? Colors.black
                      : Colors.grey),
            ),
          ),
          1: Text(
            "За все время",
            style: TextStyle(
                color: (segmentedControlGroupValue == 1)
                    ? Colors.black
                    : Colors.grey),
          ),
        },
        onValueChanged: (i) {
          setState(() {
            segmentedControlGroupValue = i;
          });
        },
      ),
    );
  }

  Widget addFriend() {
    return Container(
      child: Row(
        children: <Widget>[
          Container(
            alignment: Alignment.centerRight,
            width: MediaQuery.of(context).size.width * 0.22,
            height: 80,
            child: CircleAvatar(
              radius: 35,
              backgroundColor: Colors.green,
              child: Icon(
                Icons.add,
                color: Colors.white,
              ),
            ),
          ),
          Container(
            alignment: Alignment.centerLeft,
            height: 100,
            width: MediaQuery.of(context).size.width * 0.77,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Container(
                  padding: EdgeInsets.only(left: 20),
                  alignment: Alignment.centerLeft,
                  child: Text(
                    "Добавить друга",
                    style: TextStyle(fontSize: 20, color: Colors.green),
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  Widget title() {
    return Container(
      color: Color.fromRGBO(246, 247, 248, 1),
      height: MediaQuery.of(context).size.height * 0.08,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Container(
            width: MediaQuery.of(context).size.width * 0.5,
            child: Padding(
              padding: const EdgeInsets.all(10),
              child: Container(
                child: Text(
                  "Таблица рейтинга",
                  style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
                ),
                width: double.infinity,
              ),
            ),
          ),
          Container(
            width: MediaQuery.of(context).size.width * 0.5,
            alignment: Alignment.centerRight,
            child: Padding(
              padding: const EdgeInsets.all(20),
              child: Container(
                alignment: Alignment.centerRight,
                child: Text(
                  "${accounts?.length} друзей",
                  style: TextStyle(
                    fontSize: 24,
                    fontWeight: FontWeight.bold,
                    color: Colors.green,
                  ),
                ),
                width: double.infinity,
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget mainWidgets() {
    return Expanded(
      child: Container(
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.all(
            Radius.circular(32.0),
          ),
        ),
        child: MediaQuery.removePadding(
          context: context,
          removeTop: true,
          child: ListView(
            children: <Widget>[
              for (int i = 0; i < accounts.length; i++)
                InkWell(
                  onTap: () {},
                  child: Container(
                    padding: EdgeInsets.only(left: 10),
                    width: MediaQuery.of(context).size.width * 0.8,
                    height: 50,
                    color: Colors.white,
                    alignment: Alignment.centerLeft,
                    child: Text(
                      (accounts[i]?.fullName != null)
                          ? accounts[i]?.fullName
                          : "ss",
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 20,
                      ),
                    ),
                  ),
                ),
            ],
          ),
        ),
      ),
    );
  }

  Widget customAppBar(String title) {
    return Container(
      height: 64,
      decoration: BoxDecoration(
        color: Colors.white,
      ),
      child: Row(
        mainAxisSize: MainAxisSize.max,
        children: [
          Container(
            width: MediaQuery.of(context).size.width * 0.15,
            child: Center(
              child: InkWell(
                child: Icon(Icons.arrow_back_ios, color: Colors.green),
                onTap: () {
                  Navigator.pop(context);
                },
              ),
            ),
          ),
          Container(
            alignment: Alignment.center,
            width: MediaQuery.of(context).size.width * 0.7,
            child: InkWell(
              onTap: () {
                Navigator.pop(context);
              },
              child: Text(
                title,
                style: TextStyle(
                  fontSize: 24,
                  fontWeight: FontWeight.bold,
                ),
              ),
            ),
          ),
          Container(
            width: MediaQuery.of(context).size.width * 0.15,
            height: 22,
//            child: Image(
//              image: AssetImage("assets/icons/setting.png"),
//            ),
          ),
        ],
      ),
    );
  }
}
