import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:funtik/general/CustomFields.dart';
import 'package:funtik/general/Navigator.dart';
import 'package:funtik/pages/PasswordReminder.dart';
import 'package:funtik/pages/Registration.dart';

class LoginPage extends StatefulWidget {
  static String routeName = "/loginPage";

  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  TextEditingController emailController = new TextEditingController();
  TextEditingController passwordController = new TextEditingController();

  bool checkedValue1 = true;
  bool checkedValue2 = false;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        color: Colors.grey[100],
        child: Center(
          child: Column(
            children: [
              Container(
                decoration: BoxDecoration(
                  gradient: LinearGradient(
                    begin: Alignment.centerRight,
                    end: Alignment.centerLeft,
                    colors: <Color>[
                      Color.fromRGBO(151, 223, 71, 1),
                      Color.fromRGBO(60, 194, 74, 1),
                    ],
                  ),
                ),
                height: MediaQuery.of(context).size.height * 0.4,
                width: double.infinity,
                child: logoTitle(),
              ),
              Expanded(
//                height: MediaQuery.of(context).size.height * 0.6,
                child: ListView(
                  children: [
                    Expanded(
                      child: Container(
                        padding: EdgeInsets.only(bottom: 20),
                        alignment: Alignment.centerLeft,
                        width: double.infinity,
                        child: textFields(),
                        color: Colors.grey[100],
//                        height: 130,
                      ),
                    ),
                    Flexible(
                      child: Container(
                        color: Colors.grey[100],
//                      height: MediaQuery.of(context).size.height * 0.12,
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            checkerFields1("Пользовательское", "соглашение"),
                            Container(height: 10,),
                            checkerFields2("Политику ", "конфеденциальности"),
                          ],
                        ),
                      ),
                    ),
                    Container(
                      padding: EdgeInsets.only(bottom: 10),
                      color: Colors.grey[100],
                      height: MediaQuery.of(context).size.height * 0.2,
                      alignment: Alignment.bottomCenter,
                      width: double.infinity,
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: <Widget>[
                          CustomFields().enterButton(context, "Войти"),
                          Container(
                              padding: EdgeInsets.only(top: 10),
                              child: Center(
                                child: InkWell(
                                  onTap: () {
                                    Navigator.of(context).push(
                                      CustomNavigator().navigate(
                                        Registration(),
                                      ),
                                    );
                                  },
                                  child: Text(
                                    "Зарегистрироваться",
                                    style: TextStyle(
                                        fontSize: 16, color: Colors.green),
                                  ),
                                ),
                              ))
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget textFields() {
    return Container(
      child: Column(
        children: <Widget>[
          Container(
            alignment: Alignment.bottomLeft,
            width: MediaQuery.of(context).size.width * 0.9,
//            color: Colors.green,
            height: 20,
            padding: EdgeInsets.only(left: 10),
            child: Text(
              "Email адрес",
              style: TextStyle(fontSize: 17),
            ),
          ),
          Container(
            height: 5,
          ),
          Container(
            alignment: Alignment.bottomLeft,
            width: MediaQuery.of(context).size.width * 0.9,
            height: 50,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(8),
              boxShadow: [
                BoxShadow(
                  color: Colors.grey.withOpacity(0.2),
                  spreadRadius: 5,
                  blurRadius: 7,
                  offset: Offset(0, 3), // changes position of shadow
                ),
              ],
              color: Colors.white,
            ),
            child:
                CustomFields().emailField(emailController, context, false, ""),
          ),
          Container(
            alignment: Alignment.bottomLeft,
            width: MediaQuery.of(context).size.width * 0.9,
//            color: Colors.green,
            height: 30,
            padding: EdgeInsets.only(left: 5),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Container(
                  child: Text(
                    "Пароль",
                    style: TextStyle(fontSize: 16),
                  ),
                  padding: EdgeInsets.only(left: 10),
                ),
                InkWell(
                  onTap: () {
                    Navigator.of(context)
                        .push(CustomNavigator().navigate(PasswordReminder()));
                  },
                  child: Container(
                    padding: EdgeInsets.only(right: 10),
                    child: Text(
                      "Забыли пароль?",
                      style: TextStyle(color: Colors.grey, fontSize: 15),
                    ),
                  ),
                )
              ],
            ),
          ),
          Container(
            height: 5,
          ),
          Container(
            alignment: Alignment.centerLeft,
            width: MediaQuery.of(context).size.width * 0.9,
            height: 50,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(8),
              color: Colors.white,
              boxShadow: [
                BoxShadow(
                  color: Colors.grey.withOpacity(0.2),
                  spreadRadius: 5,
                  blurRadius: 7,
                  offset: Offset(0, 3), // changes position of shadow
                ),
              ],
            ),
            child:
                CustomFields().passwordField(passwordController, context, true),
          ),
          Container(height: 10,)
        ],
      ),
    );
  }

  Widget checkerFields1(String firstWord, String secondWord) {
    return Container(
      child: Column(
        children: <Widget>[
          Row(
            children: [
              Container(
                padding: EdgeInsets.only(left: 20),
                child: InkWell(
                  onTap: () {
                    setState(() {
                      checkedValue1 = !checkedValue1;
                    });
                  },
                  child: Container(
                    decoration: BoxDecoration(
                      shape: BoxShape.circle,
                      border: (checkedValue1)
                          ? Border.all(width: 1, color: Colors.white)
                          : Border.all(width: 1, color: Colors.grey),
                      gradient: LinearGradient(
                        begin: Alignment.centerRight,
                        end: Alignment.centerLeft,
                        colors: (checkedValue1)
                            ? [Colors.green, Colors.lightGreenAccent]
                            : [Colors.white, Colors.white],
                      ),
                    ),
                    child: Padding(
                      padding: const EdgeInsets.all(3.0),
                      child: checkedValue1
                          ? Icon(
                              Icons.check,
                              size: 17.0,
                              color: Colors.white,
                            )
                          : Icon(
                              Icons.remove,
                              size: 17.0,
                              color: Colors.white,
                            ),
                    ),
                  ),
                ),
              ),
              Container(
                width: MediaQuery.of(context).size.width * 0.8,
                height: 24,
                child: Row(
                  children: <Widget>[
                    Container(
                      padding: EdgeInsets.only(left: 20),
                      child: Text(
                        "Я принимаю ",
                        style: TextStyle(fontSize: 16),
                      ),
                    ),
                    Container(
                      child: Text(
                        firstWord,
                        style:
                            TextStyle(fontSize: 16, color: Colors.lightGreen),
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
          Container(
            alignment: Alignment.centerLeft,
            width: MediaQuery.of(context).size.width * 0.69,
            child: Text(
              secondWord,
              style: TextStyle(fontSize: 16, color: Colors.lightGreen),
            ),
          ),
        ],
      ),
    );
  }

  Widget checkerFields2(String firstWord, String secondWord) {
    return Container(
      child: Column(
        children: <Widget>[
          Row(
            children: [
              Container(
                padding: EdgeInsets.only(left: 20),
                child: InkWell(
                  onTap: () {
                    setState(() {
                      checkedValue2 = !checkedValue2;
                    });
                  },
                  child: Container(
                    decoration: BoxDecoration(
                      shape: BoxShape.circle,
                      border: (checkedValue2)
                          ? Border.all(width: 1, color: Colors.white)
                          : Border.all(width: 1, color: Colors.grey),
                      gradient: LinearGradient(
                        begin: Alignment.centerRight,
                        end: Alignment.centerLeft,
                        colors: (checkedValue2)
                            ? [Colors.green, Colors.lightGreenAccent]
                            : [Colors.white, Colors.white],
                      ),
                    ),
                    child: Padding(
                      padding: const EdgeInsets.all(3.0),
                      child: checkedValue2
                          ? Icon(
                              Icons.check,
                              size: 17.0,
                              color: Colors.white,
                            )
                          : Icon(
                              Icons.remove,
                              size: 17.0,
                              color: Colors.white,
                            ),
                    ),
                  ),
                ),
              ),
              Container(
                width: MediaQuery.of(context).size.width * 0.8,
                height: 24,
//            padding: EdgeInsets.only(left: 60),
                child: Row(
                  children: <Widget>[
                    Container(
                      padding: EdgeInsets.only(left: 20),
                      child: Text(
                        "Я принимаю ",
                        style: TextStyle(fontSize: 16),
                      ),
                    ),
                    Container(
                      child: Text(
                        firstWord,
                        style:
                            TextStyle(fontSize: 16, color: Colors.lightGreen),
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
          Container(
            alignment: Alignment.centerLeft,
            width: MediaQuery.of(context).size.width * 0.69,
            child: Text(
              secondWord,
              style: TextStyle(fontSize: 16, color: Colors.lightGreen),
            ),
          ),
        ],
      ),
    );
  }

  Widget logoTitle() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        Container(
          height: 150,
          child: Image(
            image: AssetImage('assets/icons/logo.png'),
          ),
        ),
        Container(
            height: 40,
            child: Text(
              'Логин',
              style: TextStyle(
                  color: Colors.white,
                  fontSize: 40,
                  fontWeight: FontWeight.bold),
            )),
      ],
    );
  }
}
