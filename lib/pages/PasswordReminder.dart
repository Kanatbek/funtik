import 'package:flutter/material.dart';
import 'package:funtik/general/CustomFields.dart';

class PasswordReminder extends StatefulWidget {
  @override
  _PasswordReminderState createState() => _PasswordReminderState();
}

class _PasswordReminderState extends State<PasswordReminder> {
  TextEditingController emailController = new TextEditingController();

  @override
  void initState() {
    super.initState();
    emailController.text = "konstantin.konstantinopolsky@gmail.com";
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        color: Color.fromRGBO(246, 247, 248, 1),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            customTopBar(),
            customAppBar(),
            mainWidgets(),
          ],
        ),
      ),
    );
  }

  Widget mainWidgets() {
    return Column(
      children: <Widget>[
        Container(

          color: Colors.white,
          height: MediaQuery.of(context).size.height * 0.15,
          padding: const EdgeInsets.only(top: 20, bottom: 20, left: 20),
          child: Column(
            children: [
              Container(
                alignment: Alignment.topLeft,
                child: Text(
                  "Забыли пароль?",
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 34,
                  ),
                ),
              ),
              Container(
                height: 5,
              ),
              Container(
                alignment: Alignment.bottomLeft,
                child: Text(
                  "Такое случается! Введите email, указанный при регистрации, мы отправим вам письмо.",
                  style: TextStyle(
                    fontSize: 15,
                  ),
                ),
              )
            ],
          ),
        ),
        Container(
          height: MediaQuery.of(context).size.height * 0.12,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: <Widget>[
              Container(
                alignment: Alignment.bottomLeft,
                width: double.infinity,
                height: MediaQuery.of(context).size.height * 0.05,
                padding: EdgeInsets.only(left: 40),
                child: Text(
                  "Email адрес",
                  style: TextStyle(fontSize: 16),
                ),
              ),
              Container(
                height: 10,
              ),
              Container(
                alignment: Alignment.bottomLeft,
                width: MediaQuery.of(context).size.width * 0.9,
                height: MediaQuery.of(context).size.height * 0.05,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(8),
                  color: Colors.white,
                ),
                child: CustomFields().emailField(emailController, context, false, ""),
              ),
            ],
          ),
        ),
        Container(
          height: MediaQuery.of(context).size.height * 0.5,
          alignment: Alignment.bottomCenter,
          width: double.infinity,
          child: CustomFields().enterButton(context, "Отправить"),
        ),
      ],
    );
  }

  Widget customTopBar() {
    return Container(
      height: 50,
      color: Colors.white,
    );
  }

  Widget customAppBar() {
    return Container(
      height: 64,
      decoration: BoxDecoration(
        color: Colors.white,
      ),
      child: Row(
        mainAxisSize: MainAxisSize.max,
        children: [
          Container(
            width: MediaQuery.of(context).size.width * 0.15,
            child: Center(
              child: InkWell(
                child: Icon(Icons.arrow_back_ios, color: Colors.green),
                onTap: () {
                  Navigator.pop(context);
                },
              ),
            ),
          ),
          Container(
            width: MediaQuery.of(context).size.width * 0.7,
          ),
          Container(
            width: MediaQuery.of(context).size.width * 0.15,
          ),
        ],
      ),
    );
  }
}
