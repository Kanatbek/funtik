import 'dart:ui';

import 'package:flutter/material.dart';

import 'CustomFields.dart';

class CheckerWidget extends StatefulWidget {
  bool checker = true;

  CheckerWidget({Key key, this.checker}) : super(key: key);

  @override
  _CheckerWidgetState createState() => _CheckerWidgetState();
}

class _CheckerWidgetState extends State<CheckerWidget> {
  TextEditingController smsController = new TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Container(
      child: InkWell(
        onTap: () {
          setState(() {
            widget.checker = !widget.checker;
            showPhoneDialog(context);
          });
        },
        child: Container(
          decoration: BoxDecoration(
            shape: BoxShape.circle,
            border: (widget.checker)
                ? Border.all(width: 1, color: Colors.white)
                : Border.all(width: 1, color: Colors.grey),
            gradient: LinearGradient(
              begin: Alignment.centerRight,
              end: Alignment.centerLeft,
              colors: (widget.checker)
                  ? [Colors.green, Colors.lightGreenAccent]
                  : [Colors.white, Colors.white],
            ),
          ),
          child: Padding(
            padding: const EdgeInsets.all(3.0),
            child: widget.checker
                ? Icon(
                    Icons.check,
                    size: 17.0,
                    color: Colors.white,
                  )
                : Icon(
                    Icons.remove,
                    size: 17.0,
                    color: Colors.white,
                  ),
          ),
        ),
      ),
    );
  }

  Widget showPhoneDialog(BuildContext context) {
    showModalBottomSheet(
        context: context,
        builder: (BuildContext bc) {
          return Container(
            height: MediaQuery.of(context).size.height * 0.5,
            child: Column(
              children: <Widget>[
                Container(
                  padding: EdgeInsets.only(left: 20, top: 30),
                  alignment: Alignment.topLeft,
                  child: Text(
                    "Подтвердите телефон",
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 24,
                    ),
                  ),
                ),
                customField(smsController),
                Container(
                  alignment: Alignment.topCenter,
                  height: 40,
                  child: Text(
                    "Отправить код повторно можно через 0:59 секунд",
                    style: TextStyle(fontSize: 14, color: Colors.black87),
                  ),
                ),
                Container(
                  height: 50,
                  child: CustomFields().enterButton(context, "Подтвердить"),
                ),
              ],
            ),
          );
        });
  }

  Widget customField(TextEditingController controller) {
    return Container(
      height: MediaQuery.of(context).size.height * 0.06,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: <Widget>[
          Container(
            alignment: Alignment.bottomLeft,
            width: MediaQuery.of(context).size.width * 0.9,
            height: MediaQuery.of(context).size.height * 0.05,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(8),
              color: Colors.white,
            ),
            child: CustomFields().emailField(controller, context, false, "Введите смс-код"),
          ),
        ],
      ),
    );
  }
}
