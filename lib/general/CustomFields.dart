import 'package:custom_switch/custom_switch.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:funtik/general/CheckerWidget.dart';
import 'package:funtik/general/Navigator.dart';
import 'package:funtik/pages/Registration.dart';
import 'package:funtik/pages/carousel/CarouselPage.dart';
import 'package:funtik/pages/profile/ProfilePage.dart';

class CustomFields {
  Widget emailField(TextEditingController email, BuildContext context,
      bool obscureText, String hintText) {
    return TextField(
      controller: email,
      obscureText: obscureText,
      inputFormatters: [
        LengthLimitingTextInputFormatter(12),
      ],
      onChanged: (String iinChanged) {},
      decoration: new InputDecoration(
        border: InputBorder.none,
        contentPadding: EdgeInsets.only(left: 20.0),
        hintText: hintText,
        hintStyle: TextStyle(color: Colors.grey),
        prefixText: '',
        suffixStyle: const TextStyle(
            color: Colors.green, decorationStyle: TextDecorationStyle.dotted),
      ),
      style: TextStyle(
        fontSize: 15,
        decoration: TextDecoration.none,
        letterSpacing: 1.0,
      ),
    );
  }

  Widget phoneField(
      TextEditingController email, BuildContext context, bool checker) {
    return TextField(
      controller: email,
      obscureText: false,
      textAlignVertical: TextAlignVertical.center,
      inputFormatters: [
        LengthLimitingTextInputFormatter(12),
      ],
      onChanged: (String iinChanged) {},
      decoration: new InputDecoration(
        border: InputBorder.none,
        contentPadding: EdgeInsets.only(left: 20.0),
        hintText: '',
        hintStyle: TextStyle(color: Colors.grey),
        prefixText: '',
        suffixIcon: Container(
          padding: EdgeInsets.all(10),
          child: CheckerWidget(
            checker: checker,
          ),
        ),
        suffixStyle: const TextStyle(
            color: Colors.green, decorationStyle: TextDecorationStyle.dotted),
      ),
      style: TextStyle(
        fontSize: 15,
        decoration: TextDecoration.none,
        letterSpacing: 1.0,
      ),
    );
  }

  Widget passwordField(
      TextEditingController email, BuildContext context, bool obscureText) {
    return TextField(
      controller: email,
      obscureText: obscureText,
      textAlignVertical: TextAlignVertical.center,
      inputFormatters: [
        LengthLimitingTextInputFormatter(12),
      ],
      onChanged: (String iinChanged) {},
      decoration: new InputDecoration(
        suffixIcon: Icon(
          Icons.remove_red_eye,
          color: Colors.grey,
        ),
        contentPadding: EdgeInsets.only(left: 20.0),
        border: InputBorder.none,
        hintText: '',
        hintStyle: TextStyle(color: Colors.grey),
        prefixText: ' ',
        suffixStyle: const TextStyle(
            color: Colors.green, decorationStyle: TextDecorationStyle.dotted),
      ),
      style: TextStyle(
        fontSize: 15,
        decoration: TextDecoration.none,
        letterSpacing: 1.0,
      ),
    );
  }

  Widget enterButton(BuildContext context, String buttonTitle) {
    return FlatButton(
      splashColor: Colors.white,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(8.0),
      ),
      child: Container(
        height: 47,
        width: double.infinity,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(30),
          gradient: LinearGradient(
            colors: <Color>[
              Colors.green,
              Colors.green,
            ],
          ),
        ),
        child: Center(
          child: Text(
            buttonTitle,
            style: TextStyle(
              fontSize: MediaQuery.of(context).size.width * 0.05,
              color: Colors.white,
              fontWeight: FontWeight.bold,
            ),
          ),
        ),
      ),
      onPressed: () => {
        Navigator.of(context).push(
          CustomNavigator().navigate(
            ProfilePage(),
          ),
        ),
      },
    );
  }

  Widget enterButton2(BuildContext context, String buttonTitle) {
    return FlatButton(
      splashColor: Colors.white,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(8.0),
      ),
      child: Container(
        height: 60,
        width: double.infinity,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(30),
          border: Border.all(width: 1, color: Colors.redAccent),
          color: Colors.white,
        ),
        child: Center(
          child: Text(
            buttonTitle,
            style: TextStyle(
              fontSize: MediaQuery.of(context).size.width * 0.05,
              color: Colors.redAccent,
              fontWeight: FontWeight.bold,
            ),
          ),
        ),
      ),
      onPressed: () => {
        Navigator.of(context).push(
          CustomNavigator().navigate(
            ProfilePage(),
          ),
        ),
      },
    );
  }

  Widget customSelector(String title, BuildContext context) {
    return Container(
      padding: EdgeInsets.only(left: 20, right: 20),
      width: MediaQuery.of(context).size.width * 1,
      height: 60,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Padding(
            padding: const EdgeInsets.only(top: 20),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Text(
                  title,
                  style: TextStyle(fontSize: 16),
                ),
                Icon(
                  Icons.arrow_forward_ios_outlined,
                  color: Colors.grey[300],
                ),
              ],
            ),
          ),
          Container(
            height: 5,
            decoration: BoxDecoration(
              border: Border(
                bottom: BorderSide(
                  width: 1.0,
                  color: Color.fromRGBO(246, 247, 248, 1),
                ),
              ),
              color: Colors.white,
            ),
          ),
        ],
      ),
    );
  }

  Widget customChecker(String title, BuildContext context) {
    return Container(
      padding: EdgeInsets.only(left: 20, right: 20),
      width: MediaQuery.of(context).size.width * 1,
      height: 70,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Padding(
            padding: const EdgeInsets.only(top: 20),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Text(
                  title,
                  style: TextStyle(fontSize: 16),
                ),
                CupertinoSwitch(
                  value: true,
                  onChanged: null,
                ),
              ],
            ),
          ),
          Container(
            height: 5,
            decoration: BoxDecoration(
              border: Border(
                bottom: BorderSide(
                  width: 1.0,
                  color: Color.fromRGBO(246, 247, 248, 1),
                ),
              ),
              color: Colors.white,
            ),
          ),
        ],
      ),
    );
  }

  Widget customTitle(String title, BuildContext context) {
    return Container(
      padding: EdgeInsets.only(left: 20, right: 20),
      color: Color.fromRGBO(246, 247, 248, 1),
      width: MediaQuery.of(context).size.width * 1,
      height: 60,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Padding(
            padding: const EdgeInsets.only(top: 20),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Text(
                  title,
                  style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
