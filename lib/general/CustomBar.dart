import 'package:flutter/material.dart';

class CustomSlider extends StatelessWidget {
  final double percentage;
  final double width;

  CustomSlider({
    this.percentage,
    this.width,
  });

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Column(
        children: [
          Container(
            width: double.infinity,
            height: 50,
          ),
          Container(
            color: Colors.blue,
              height: percentage == null ? 0 : width * percentage,
            width: 50,
            child: Align(
              alignment: Alignment.centerLeft,
              child: Text(
                percentage == null ? "0" : (percentage * 100).toString(),
              ),
            ),
          ),
        ],
      ),
    );
  }
}