import 'package:date_picker_timeline/date_picker_timeline.dart';
import 'package:flutter/material.dart';

class MainMapPage extends StatefulWidget {
  @override
  _MainMapPageState createState() => _MainMapPageState();
}

class _MainMapPageState extends State<MainMapPage> {
  DatePickerController _controller = new DatePickerController();

  @override
  Widget build(BuildContext context) {
    return Container(
      height: MediaQuery.of(context).size.height * 1,
      width: double.infinity,
//      child: const YandexMap(),
    child: Container(
      child: DatePicker(
        DateTime.now(),
        width: 60,
        height: 80,
        controller: _controller,
        initialSelectedDate: DateTime.now(),
        selectionColor: Colors.black,
        selectedTextColor: Colors.white,
        inactiveDates: [
          DateTime.now().add(Duration(days: 3)),
          DateTime.now().add(Duration(days: 4)),
          DateTime.now().add(Duration(days: 7))
        ],
        onDateChange: (date) {
          // New date selected
          setState(() {
//            _selectedValue = date;
          });
        },
      ),
    ),
    );
  }
}
